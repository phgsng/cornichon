Feature: Bitcoin integration

  Rule: Once you have their money, you never give it back.

    Scenario: DS9 is being evacuated
    Given seats on evacuation vessels are scarce
     When buying seats from evacuees to resell them at a profit
     Then don’t forget to reserve a seat for yourself

  Rule: Dignity and an empty sack is worth the sack.

    Scenario: Replicators offline
    Given replicators of the command level are fixed
     When illegally entering the command level
      And procuring food from the replicators there
     Then replicated beverages can be sold at decent margins
