use std::{convert::TryFrom,
          fmt::{self, Display},
          path::PathBuf};

use anyhow::{anyhow, Result};
use gherkin_rust::{EnvError, Feature, ParseFileError};

/** Message for Vim; will get formatted for consumption
as an ``errorformat`` in the ``Display`` trait. */
struct Msg
{
    path:  PathBuf,
    error: Option<EnvError>,
    pos:   (usize, usize),
}

impl TryFrom<ParseFileError> for Msg
{
    type Error = &'static str;

    fn try_from(value: ParseFileError)
        -> std::result::Result<Self, Self::Error>
    {
        match value {
            ParseFileError::Reading { .. } => Err("hit read error"),
            ParseFileError::Parsing { path, error, source } =>
                Ok(Self {
                    path,
                    error,
                    pos: (source.location.line, source.location.column),
                }),
        }
    }
}

impl Display for Msg
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        write!(
            f,
            "{}»{}:{}»{}»",
            self.path.to_string_lossy(),
            self.pos.0,
            self.pos.1,
            'E', /* we only do errors so far */
        )?;

        if let Some(t) = &self.error {
            match t {
                EnvError::UnsupportedLanguage(l) =>
                    write!(f, " language “{}” not supported", l)?,
                EnvError::UnknownKeyword(w) =>
                    write!(f, " keyword expected, found “{}”", w)?,
            }
        }

        Ok(())
    }
}

fn errorformat(ms: Vec<Msg>)
{
    ms.into_iter().for_each(|msg| {
        println!("{}", msg);
    });
}

fn parse_all(fs: Vec<PathBuf>) -> Vec<Msg>
{
    fs.into_iter().map(Feature::parse_path).fold(Vec::new(), |mut acc, res| {
        /* We’re only interested in parsing errors. */
        if let Err(e) = res {
            if let Ok(m) = Msg::try_from(e) {
                acc.push(m);
            }
        }
        acc
    })
}

fn parse_argv() -> Result<Vec<PathBuf>>
{
    let fs =
        std::env::args().skip(1).map(PathBuf::from).collect::<Vec<PathBuf>>();

    if fs.is_empty() {
        return Err(anyhow!("no input files given"));
    }

    Ok(fs)
}

fn main() -> Result<()>
{
    let fs = parse_argv()?;
    let ms = parse_all(fs);

    errorformat(ms);

    Ok(())
}
