" Language:  Gherkin
"   Author:  Philipp Gesang <phg@phi-gamma.net>
"  License:  Vim license
"  Version:  0.1.42

if exists('g:cornichon_autoloaded')
    finish
endif

let s:cpo_save = &cpoptions
set cpoptions&vim

let g:cornichon_autoloaded = 1
let s:show_errors          = 0

sign define CCN_Error   text=>> texthl=Error
sign define CCN_Warning text=>> texthl=Todo

fun cornichon#Add_sign(error_id, error)
    let type = a:error.type == "W" ? "CCN_Warning" : "CCN_Error"
    if a:error.lnum > 0
        execute "sign place" a:error_id "line=" . a:error.lnum "name=" . type "buffer=" . a:error.bufnr
    endif
endfun

let g:ccn_errs_of_buf = { }

fun cornichon#Proc_qf(bufno, qflist)
    sign unplace 42
    for fixme in a:qflist
        if fixme.bufnr == 0
            continue
        endif
        let lnum = fixme.lnum > 1 ? fixme.lnum - 1 : 0
        let type = fixme.type == 'E' ? "CCN_Error" : "CCN_Warning"
        execute "sign place 42 line=" . lnum . " name=" . type . " buffer=" . fixme.bufnr
    endfor
endfun

fun cornichon#Buf_write(bufno)
    try
        compiler cornichon
        setlocal shellpipe=>
        execute "silent make!" shellescape(bufname(a:bufno), 1)
        call cornichon#Proc_qf(a:bufno, getqflist())
    endtry
endfun

fun cornichon#Enable()
    augroup CCN
        autocmd!
        "autocmd CursorHold,CursorMoved *.feature
                    "\ call cornichon#err_msg(expand("<abuf>")+0, getpos("."))
        autocmd BufWritePost *.feature call cornichon#Buf_write(expand("<abuf>")+0)
        "autocmd BufDelete    *.feature call cornichon#Unload(expand("<abuf>")+0)
    augroup END
    let s:show_errors = 1
endfun

let &cpoptions = s:cpo_save
unlet s:cpo_save
