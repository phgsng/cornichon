" Language:  Gherkin
"   Author:  Philipp Gesang <phg@phi-gamma.net>
"  License:  Vim license
"  Version:  0.1.42

let s:self = "cornichon"

if exists("current_compiler")
  finish
endif

let current_compiler = s:self

let s:cpo_save = &cpoptions
set cpoptions&vim

if exists(":CompilerSet") != 2
  command -nargs=* CompilerSet setlocal <args>
endif

" assume the cornichon binary is the same path as this script
let g:ccn_binary = expand("<sfile>:h")
            \ . '/../../target/debug/'
            \ . s:self

execute "CompilerSet makeprg=" 
            \ . escape(fnameescape(g:ccn_binary), ' \')
            \ . '\ %:S'

" foo.feature»1337:42»W» some warning
CompilerSet errorformat=%f»%l:%c»%t»\ %m

let &cpoptions = s:cpo_save
unlet s:cpo_save
