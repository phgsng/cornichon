if exists('g:cornichon_loaded')
    finish
endif

let g:cornichon_loaded = 1

if !exists("g:cornichon_enable") || g:cornichon_enable
    call cornichon#Enable()
endif

command CCNEnable  call cornichon#Enable()
"command CCNDisable call cornichon#Disable()
"command CCNToggle  call cornichon#Toggle()
